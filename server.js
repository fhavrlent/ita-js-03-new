const express = require('express')
const _ = require('lodash')
const app = express()
const bodyParser = require("body-parser");
const cors = require("cors")
const passport = require("passport");
const path = require("path");
const knex = require('knex');
const {Model} = require('objection');


const db = knex({
  client: 'pg',
  connection: {
    host: 'ec2-54-75-249-162.eu-west-1.compute.amazonaws.com',
    user: 'vdfvwuoudhxvhx',
    password: '',
    database: 'd6dbg4t17f77eq',
    ssl: true
  }
})

Model.knex(db)


const port = 8080;

//routes
const contacts = require("./routes/contacts")
//

app.use(passport.initialize())
app.use(passport.session())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))
app.use(cors())
app.use(express.static(path.join(__dirname, "react-src/build")))
//Routes
app.use('/api/contacts/', contacts)
app.get("*", (req, res)=>{
    res.sendFile(path.join(__dirname, "react-src/build/index.html"))
  })
//

app.listen(port, ()=>{
  console.log("Server running on port " + port)
})

