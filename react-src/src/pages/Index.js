import React from 'react';
import Index from '../components/Index/Index';

class IndexPage extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      users: []
    }
  }

  render() {
    const { users } = this.state;
    return (
      <div>
        <Index users={users}/>
      </div>
    )
  }
}

export default IndexPage;
