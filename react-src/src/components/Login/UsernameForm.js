import React  from 'react';
import {FormGroup, ControlLabel, FormControl} from 'react-bootstrap';

function UsernameForm () {
  return (
    <FormGroup controlId='username'>
      <ControlLabel>Username: </ControlLabel>
      <FormControl
        type='text'
        name='username'
      />
    </FormGroup>
  );
}

export default UsernameForm;
