import React from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { IndexLink } from 'react-router';

function AppNavbar() {
  return (
    <Navbar >
      <Navbar.Header>
        <Navbar.Brand>
          <IndexLink to='/'>App</IndexLink>
        </Navbar.Brand>
        <Navbar.Toggle />
      </Navbar.Header>
      <Navbar.Collapse>
        <Nav>
          <NavItem eventKey={1} href="/add">Add contact</NavItem>
        </Nav>
        <Nav pullRight>
          <NavItem eventKey={1} href="/login">Login</NavItem>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

export default AppNavbar;

