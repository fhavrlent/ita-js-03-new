import React from 'react';
import {Link} from 'react-router';

function TableEntry({id, name, address, note, phone, }) {
  return (
    <tr>
      <td><Link to={`/user/${id}/`}>{name}</Link></td>
      <td>{phone}</td>
      <td>{address}</td>
      <td>{note}</td>
    </tr>
  );
}

export default TableEntry;
