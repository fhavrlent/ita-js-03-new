import React from 'react';


function Add() {
  return (
    <div className="container">
      <h2>New Contact</h2>
      <div className="row">
        <div className="col-md-6">
          <div className="form-group">
            <label htmlFor="">Name</label>
            <input type="text" className="form-control" />
          </div>
          <div className="form-group">
            <label htmlFor="">Phone</label>
            <input type="text" className="form-control" />
          </div>
          <div className="form-group">
            <label htmlFor="">Address</label>
            <textarea className="form-control"></textarea>
          </div>
        </div>
        <div className="col-md-6">
          <div className="form-group">
            <label htmlFor="">Note</label>
            <textarea className="form-control"></textarea>
          </div>
        </div>
      </div>
      <div>
        <a href="/" className="btn btn-default">Save</a>
      </div>
    </div>
  );
}

export default Add;

