import React  from 'react';
import {Button} from 'react-bootstrap';

function SubmitButton ({text}) {
  return (
    <Button type='submit' bsStyle='success'>{text}</Button>
  );
}


export default SubmitButton;
