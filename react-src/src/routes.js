import React from 'react';
import { IndexRoute, Route } from 'react-router';
import AppPage from './pages/App';
import IndexPage from './pages/Index';
import LoginPage from './pages/Login';
import AddPage from './pages/Add';
import DetailsPage from './pages/Details';

export default (
  <Route path='/' component={AppPage}>
    <IndexRoute component={IndexPage} />
    <Route path='login' component={LoginPage} />
    <Route path='add' component={AddPage} />
    <Route path='user/:slug' component={DetailsPage} />
    <Route path='*' component={IndexPage} />
  </Route>
);
